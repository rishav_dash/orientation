# Dockers Summary

## - What is Dockers Image

![Doc image](extras/dockers_image.png)

## - What is Dockers Container

![Doc Container](extras/doc_container.png)

## - How to install Dockers

![Doc install](extras/doc_install.PNG)

## - Basic Dockers commands

![Doc commands](extras/doc_commands.jpg)



