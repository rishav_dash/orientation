# Git Summary
### - Getting a Git Repository
This is a way in which you can set up a git Repository.
![git repo](extras/git_repo.png)

### - Recording Changes to the Repository
This is the way to get changes in your Repository.

![git changes](extras/git_changes.png)

and also This

![git changes 1](extras/git_changes_1.jpg)

### - GitHub Push Pull command

![git push pull](extras/git_push.jpg)

